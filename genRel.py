import openpyxl as px
from util import *

#################################################
# Generate relationships
#################################################

# 1.Location: Event -> Place
def genRelLocation(p, startRow, endRow):
  File = open('./cql/rel/genRelLocation.cql', 'w')
  res = set()
  for rowId, row in enumerate(p.iter_rows()):
    if rowId < startRow-1:
      continue
    elif rowId >= endRow-1:
      break
    nNodeStr = getEventStr(row) #1
    mNodeStr = getPlaceStr(row) #2
    relation = "Location"
    res.add(FORMAT_CREATE_REL % (nNodeStr, mNodeStr, relation))
  for k in res:
    print >> File, k
  File.close()    

# 2.LocatedIn: Place -> Country Name
def genRelLocatedIn(p, startRow, endRow):
  File = open('./cql/rel/genRelLocatedIn.cql', 'w')
  res = set()
  for rowId, row in enumerate(p.iter_rows()):
    if rowId < startRow-1:
      continue
    elif rowId >= endRow-1:
      break
    nNodeStr = getPlaceStr(row) #1
    mNodeStr = getCountryStr(row) #2
    relation = "LocatedIn"
    res.add(FORMAT_CREATE_REL % (nNodeStr, mNodeStr, relation))
  for k in res:
    print >> File, k
  File.close()    

# 3.OccurredAt: Event -> Site
def genRelOccurredAt(p, startRow, endRow):
  File = open('./cql/rel/genRelOccurredAt.cql', 'w')
  res = set()
  for rowId, row in enumerate(p.iter_rows()):
    if rowId < startRow-1:
      continue
    elif rowId >= endRow-1:
      break
    nNodeStr = getEventStr(row) #1
    mNodeStr = getSiteStr(row) #2
    relation = "OccurredAt"
    res.add(FORMAT_CREATE_REL % (nNodeStr, mNodeStr, relation))
  for k in res:
    print >> File, k
  File.close()    


# 4.LocatedIn: Site->Place
def genRelLocatedIn2(p, startRow, endRow):
  File = open('./cql/rel/genRelLocatedIn2.cql', 'w')
  res = set()
  for rowId, row in enumerate(p.iter_rows()):
    if rowId < startRow-1:
      continue
    elif rowId >= endRow-1:
      break
    nNodeStr = getSiteStr(row) #1
    mNodeStr = getPlaceStr(row) #2
    relation = "LocatedIn"
    res.add(FORMAT_CREATE_REL % (nNodeStr, mNodeStr, relation))
  for k in res:
    print >> File, k
  File.close()    

# 5 AttackType: Event->Attack
def genRelAttackType(p, startRow, endRow):
  File = open('./cql/rel/genRelAttackType.cql', 'w')
  res = set()
  for rowId, row in enumerate(p.iter_rows()):
    if rowId < startRow-1:
      continue
    elif rowId >= endRow-1:
      break
    eventNodeStr = getEventStr(row)  #1
    for attackCellStr in ["AD", "AF", "AH"]:
      attackStr = formatStrForLabel(getCellStr(row[calOffset(attackCellStr)]))
      if attackStr == "":
        continue
      for weaponCellStr, weaponSubCellStr in zip(["CD", "CH", "CL", "CP"], ["CF", "CJ", "CN", "CR"]):
        weaponStr = getCellStr(row[calOffset(weaponCellStr)])
        weaponSubStr = getCellStr(row[calOffset(weaponSubCellStr)])
        if weaponStr == "":
          continue
        attackNodeStr = getAttackStr(row, attackStr, weaponStr, weaponSubStr)  #2
        res.add(FORMAT_CREATE_REL % (eventNodeStr, attackNodeStr, "AttackType"))
  for k in res:
    print >> File, k
  File.close()    

# 6 AttackTarget: Attack -> Target
def genRelAttackTarget(p, startRow, endRow):
  File = open('./cql/rel/genRelAttackTarget.cql', 'w')
  res = set()
  relation = "AttackTarget"
  for rowId, row in enumerate(p.iter_rows()):
    if rowId < startRow-1:
      continue
    elif rowId >= endRow-1:
      break
    for attackCellStr in ["AD", "AF", "AH"]:
      attackStr = formatStrForLabel(getCellStr(row[calOffset(attackCellStr)]))
      if attackStr == "":
        continue
      for weaponCellStr, weaponSubCellStr in zip(["CD", "CH", "CL", "CP"], ["CF", "CJ", "CN", "CR"]):
        weaponStr = getCellStr(row[calOffset(weaponCellStr)])
        weaponSubStr = getCellStr(row[calOffset(weaponSubCellStr)])
        if weaponStr == "":
          continue
        nNodeStr = getAttackStr(row, attackStr, weaponStr, weaponSubStr)  #1
        for targetCellStr, targetSubCellStr, victimCellStr, corpCellStr, natltyCellStr in zip(["AJ", "AR", "AZ"], ["AL", "AT", "BB"], ["AN", "AV", "BD"], ["AM", "AU", "BC"], ["AP", "AX", "BF"]):
          targetStr = formatStrForLabel(getCellStr(row[calOffset(targetCellStr)]))
          if targetStr == "":
            continue
          targetSubStr = getCellStr(row[calOffset(targetSubCellStr)])
          victimStr = getCellStr(row[calOffset(victimCellStr)])
          corpStr = getCellStr(row[calOffset(corpCellStr)])
          natltyStr = getCellStr(row[calOffset(natltyCellStr)])
          mNodeStr = getTargetStr(targetStr, targetSubStr, victimStr, corpStr, natltyStr)  #2
          res.add(FORMAT_CREATE_REL % (nNodeStr, mNodeStr, relation))
  for k in res:
    print >> File, k
  File.close()    

# 7 Agent:Event->Perpetrator
def genRelAgent(p, startRow, endRow):
  File = open('./cql/rel/genRelAgent.cql', 'w')
  res = set()
  relation = "Agent"
  for rowId, row in enumerate(p.iter_rows()):
    if rowId < startRow-1:
      continue
    elif rowId >= endRow-1:
      break
    nNodeStr = getEventStr(row) #1
    for gnameCellStr, gnameSubCellStr, gcertCellStr in zip(["BG", "BI", "BK"], ["BH", "BJ", "BL"], ["BN", "BO", "BP"]):
      gnameStr = getCellStr(row[calOffset(gnameCellStr)])
      if gnameStr == "":
        continue
      gnameSubStr = getCellStr(row[calOffset(gnameSubCellStr)])
      mNodeStr = getPerpetratorStr(row, gnameStr, gnameSubStr, gcertCellStr) #2
      res.add(FORMAT_CREATE_REL % (nNodeStr, mNodeStr, relation))
  for k in res:
    print >> File, k
  File.close()    

# 8. HumanDamage: Event -> Victims
def genRelHumanDamage(p, startRow, endRow):
  File = open('./cql/rel/genRelHumanDamage.cql', 'w')
  res = set()
  relation = "HumanDamage"
  for rowId, row in enumerate(p.iter_rows()):
    if rowId < startRow-1:
      continue
    elif rowId >= endRow-1:
      break
    nNodeStr = getEventStr(row) #1
    mNodeStr = getVictimsStr(row) #2
    res.add(FORMAT_CREATE_REL % (nNodeStr, mNodeStr, relation))
  for k in res:
    print >> File, k
  File.close()    

# 9 PropertyDestroyed: Event -> PropertyDamage
def genRelPropertyDestroyed(p, startRow, endRow):
  File = open('./cql/rel/genRelPropertyDestroyed.cql', 'w')
  res = set()
  relation = "PropertyDestroyed"
  for rowId, row in enumerate(p.iter_rows()):
    if rowId < startRow-1:
      continue
    elif rowId >= endRow-1:
      break
    nNodeStr = getEventStr(row) #1
    mNodeStr = getPropertyDamageStr(row) #2
    res.add(FORMAT_CREATE_REL % (nNodeStr, mNodeStr, relation))
  for k in res:
    print >> File, k
  File.close()    

# 10 HostageSituation: Event -> Hostages_Kidnaps
def genRelHostageSituation(p, startRow, endRow):
  File = open('./cql/rel/genRelHostageSituation.cql', 'w')
  res = set()
  relation = "HostageSituation"
  for rowId, row in enumerate(p.iter_rows()):
    if rowId < startRow-1:
      continue
    elif rowId >= endRow-1:
      break
    nNodeStr = getEventStr(row) #1
    mNodeStr = getHostagesKidnapsStr(row) #2
    res.add(FORMAT_CREATE_REL % (nNodeStr, mNodeStr, relation))
  for k in res:
    print >> File, k
  File.close()    

# 11 DiversionCountry: Hostage -> Country
def genRelDiversionCountry(p, startRow, endRow):
  File = open('./cql/rel/genRelDiversionCountry.cql', 'w')
  res = set()
  relation = "DiversionCountry"
  for rowId, row in enumerate(p.iter_rows()):
    if rowId < startRow-1:
      continue
    elif rowId >= endRow-1:
      break
    nNodeStr = getHostagesKidnapsStr(row) #1
    mNodeStr = getDiversionCountryStr(row)  #2
    if mNodeStr == "":
      continue;
    res.add(FORMAT_CREATE_REL % (nNodeStr, mNodeStr, relation))
  for k in res:
    print >> File, k
  File.close()    

# 12  ResolutionCountry: Hostage -> Country
def genRelResolutionCountry(p, startRow, endRow):
  File = open('./cql/rel/genRelResolutionCountry.cql', 'w')
  res = set()
  relation = "ResolutionCountry"
  for rowId, row in enumerate(p.iter_rows()):
    if rowId < startRow-1:
      continue
    elif rowId >= endRow-1:
      break
    nNodeStr = getHostagesKidnapsStr(row) #1
    mNodeStr = getResolutionCountryStr(row)  #2
    if mNodeStr == "":
      continue
    res.add(FORMAT_CREATE_REL % (nNodeStr, mNodeStr, relation))
  for k in res:
    print >> File, k
  File.close()    

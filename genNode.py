import openpyxl as px
from collections import OrderedDict
from util import *

#################################################
# Generate Nodes
#################################################

# generate event
def genEvent(p, startRow, endRow):
  File = open('./cql/node/genEvent.cql', 'w')
  res = []
  for rowId, row in enumerate(p.iter_rows()):
    if rowId < startRow-1:
      continue
    elif rowId >= endRow-1:
      break
    res.append(FORMAT_CREATE_NODE % getEventStr(row))
      
  for k in list(OrderedDict.fromkeys(res)):
    print >> File, k
  File.close()    

# generate Country
def genCountry(p, startRow, endRow):
  File = open('./cql/node/genCountry.cql', 'w')
  res = set()
  for rowId, row in enumerate(p.iter_rows()):
    if rowId < startRow-1:
      continue
    elif rowId >= endRow-1:
      break
    res.add(FORMAT_CREATE_NODE % getCountryStr(row))  
  for k in res:
    print >> File, k
  File.close()    

# generate Place
def genPlace(p, startRow, endRow):
  File = open('./cql/node/genPlace.cql', 'w')
  res = set()
  for rowId, row in enumerate(p.iter_rows()):
    if rowId < startRow-1:
      continue
    elif rowId >= endRow-1:
      break
    res.add(FORMAT_CREATE_NODE % getPlaceStr(row))  
  for k in res:
    print >> File, k
  File.close()    

# generate Site
def genSite(p, startRow, endRow):
  File = open('./cql/node/genSite.cql', 'w')
  res = set()
  for rowId, row in enumerate(p.iter_rows()):
    if rowId < startRow-1:
      continue
    elif rowId >= endRow-1:
      break
    res.add(FORMAT_CREATE_NODE % getSiteStr(row))  
  for k in res:
    print >> File, k
  File.close()    

# generate Attack
def genAttack(p, startRow, endRow):
  File = open('./cql/node/genAttack.cql', 'w')
  res = set()
  for rowId, row in enumerate(p.iter_rows()):
    if rowId < startRow-1:
      continue
    elif rowId >= endRow-1:
      break
    for attackCellStr in ["AD", "AF", "AH"]:
      attackStr = formatStrForLabel(getCellStr(row[calOffset(attackCellStr)]))
      if attackStr == "":
        continue
      for weaponCellStr, weaponSubCellStr in zip(["CD", "CH", "CL", "CP"], ["CF", "CJ", "CN", "CR"]):
        weaponStr = getCellStr(row[calOffset(weaponCellStr)])
        weaponSubStr = getCellStr(row[calOffset(weaponSubCellStr)])
        if weaponStr == "":
          continue
        res.add(FORMAT_CREATE_NODE % getAttackStr(row, attackStr, weaponStr, weaponSubStr))
  for k in res:
    print >> File, k
  File.close()    

# generate Target
def genTarget(p, startRow, endRow):
  File = open('./cql/node/genTarget.cql', 'w')
  res = set()
  for rowId, row in enumerate(p.iter_rows()):
    if rowId < startRow-1:
      continue
    elif rowId >= endRow-1:
      break
    for targetCellStr, targetSubCellStr, victimCellStr, corpCellStr, natltyCellStr in zip(["AJ", "AR", "AZ"], ["AL", "AT", "BB"], ["AN", "AV", "BD"], ["AM", "AU", "BC"], ["AP", "AX", "BF"]):
      targetStr = formatStrForLabel(getCellStr(row[calOffset(targetCellStr)]))
      if targetStr == "":
        continue
      targetSubStr = getCellStr(row[calOffset(targetSubCellStr)])
      victimStr = getCellStr(row[calOffset(victimCellStr)])
      corpStr = getCellStr(row[calOffset(corpCellStr)])
      natltyStr = getCellStr(row[calOffset(natltyCellStr)])
      res.add(FORMAT_CREATE_NODE % getTargetStr(targetStr, targetSubStr, victimStr, corpStr, natltyStr))
  for k in res:
    print >> File, k
  File.close()    

# generate Perpetrator
def genPerpetrator(p, startRow, endRow):
  File = open('./cql/node/genPerp.cql', 'w')
  res = set()
  for rowId, row in enumerate(p.iter_rows()):
    if rowId < startRow-1:
      continue
    elif rowId >= endRow-1:
      break
    for gnameCellStr, gnameSubCellStr, gcertCellStr in zip(["BG", "BI", "BK"], ["BH", "BJ", "BL"], ["BN", "BO", "BP"]):
      gnameStr = getCellStr(row[calOffset(gnameCellStr)])
      if gnameStr == "":
        continue
      gnameSubStr = getCellStr(row[calOffset(gnameSubCellStr)])
      res.add(FORMAT_CREATE_NODE % getPerpetratorStr(row, gnameStr, gnameSubStr, gcertCellStr))
  for k in res:
    print >> File, k
  File.close()    

# generate Victims
def genVictims(p, startRow, endRow):
  File = open('./cql/node/genVictims.cql', 'w')
  res = set()
  for rowId, row in enumerate(p.iter_rows()):
    if rowId < startRow-1:
      continue
    elif rowId >= endRow-1:
      break
    res.add(FORMAT_CREATE_NODE % getVictimsStr(row))
  for k in res:
    print >> File, k
  File.close()    

# generate PropertyDamage
def genPropertyDamage(p, startRow, endRow):
  File = open('./cql/node/genPropertyDamage.cql', 'w')
  res = set()
  for rowId, row in enumerate(p.iter_rows()):
    if rowId < startRow-1:
      continue
    elif rowId >= endRow-1:
      break
    res.add(FORMAT_CREATE_NODE % getPropertyDamageStr(row))
  for k in res:
    print >> File, k
  File.close()    

# generate HostagesKidnaps
def genHostagesKidnaps(p, startRow, endRow):
  File = open('./cql/node/genHostagesKidnaps.cql', 'w')
  res = set()
  for rowId, row in enumerate(p.iter_rows()):
    if rowId < startRow-1:
      continue
    elif rowId >= endRow-1:
      break
    res.add(FORMAT_CREATE_NODE % getHostagesKidnapsStr(row))
  for k in res:
    print >> File, k
  File.close()    


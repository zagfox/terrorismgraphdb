#!/bin/bash

neo4jShell="/Users/fengzhu/Software/neo4j-community-2.1.5/bin/neo4j-shell"
nodeCQL=$(find cql/node/ -name "*.cql")
relCQL=$(find cql/rel/ -name "*.cql")

# delete db
$neo4jShell -c "MATCH (n) OPTIONAL MATCH (n)-[r]-() DELETE n,r;"

# generate node
for cql in $nodeCQL; do
	$neo4jShell -file $cql
done

# generate rel
for cql in $relCQL; do
	$neo4jShell -file $cql
done



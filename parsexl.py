import openpyxl as px
import time
import genNode as gNode
import genRel as gRel
from util import *

w = px.load_workbook('globalterrorismdb_0814dist.xlsx', use_iterators = True)
p = w.get_sheet_by_name('Data')
print "readFile done"

startRow = 2;
endRow = 1002;

# main func
if __name__ == "__main__":
  t1 = time.clock()
  gNode.genEvent(p, startRow, endRow)
  print "1 genEvent done"
  gNode.genCountry(p, startRow, endRow)
  print "2 genCountry done"
  gNode.genPlace(p, startRow, endRow)
  print "3 genPlace done"
  gNode.genSite(p, startRow, endRow)
  print "4 genSite done"
  gNode.genAttack(p, startRow, endRow)
  print "5 genAttack done"
  gNode.genTarget(p, startRow, endRow)
  print "6 genTarget done"
  gNode.genPerpetrator(p, startRow, endRow)
  print "7 genPerpetrator done"
  gNode.genVictims(p, startRow, endRow)
  print "8 genVictims done"
  gNode.genPropertyDamage(p, startRow, endRow)
  print "9 genPropertyDamage done"
  gNode.genHostagesKidnaps(p, startRow, endRow)
  print "10 genHostagesKidnaps done"

  gRel.genRelLocation(p, startRow, endRow)
  print "1 genRelLocation done"
  gRel.genRelLocatedIn(p, startRow, endRow)
  print "2 genRelLocatedIn done"
  gRel.genRelOccurredAt(p, startRow, endRow)
  print "3 genRelOccurredAt done"
  gRel.genRelLocatedIn2(p, startRow, endRow)
  print "4 genRelLocatedIn2 done"
  gRel.genRelAttackType(p, startRow, endRow)
  print "5 genRelAttackType done"
  gRel.genRelAttackTarget(p, startRow, endRow)
  print "6 genRelAttarckTarget done"
  gRel.genRelAgent(p, startRow, endRow)
  print "7 genRelAgent done"
  gRel.genRelHumanDamage(p, startRow, endRow)
  print "8 genRelHumanDamage done"
  gRel.genRelPropertyDestroyed(p, startRow, endRow)
  print "9 genRelPropertyDestroyed done"
  gRel.genRelHostageSituation(p, startRow, endRow)
  print "10 genRelHostageSituation done"
  gRel.genRelDiversionCountry(p, startRow, endRow)
  print "11 genRelDiversionCountry done"
  gRel.genRelResolutionCountry(p, startRow, endRow)
  print "12 genRelResolutionCountry done"

  t2 = time.clock()
  print "%.2gs" % (t2-t1)


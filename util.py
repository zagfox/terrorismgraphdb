import openpyxl as px

# get string, format it from excel cell
def getCellStr(cell):
  if cell.value is None:
    return ""
  else:
    return (formatStrForQuote(cell.value)).encode('utf8')

def getCellDate(cell):
  if cell.value is None:
    return ""
  else:
    return (cell.value).strftime("%m-%d-%Y")

def getCellFloat(cell):
  if cell.value is None:
    return -9999.00;
  else:
    return float(cell.value)

def getCellInt(cell):
  if cell.value is None:
    return -9999;
  else:
    return int(cell.value)

def formatStrForLabel(s):
  return s.replace(' ', '').replace('/', '').replace('&', '').replace('(', '_').replace(')', '_').replace('-', '_')

# chagne doule quote to single quote
def formatStrForQuote(s):
  return s.replace('\"', '\'').replace('\/', '\_')

########################################################
# Format of entity
EVENT_FORMAT = ":Event {EventID:%d, Year:%d, Month:%d, Day:%d, ApproximateDate:\"%s\", isExtended:%d, resolutionDate:\"%s\", summary:\"%s\", motive:\"%s\", isProperTerrorism:%d, alternative:\"%s\", isMultiAttack:%d, afterMath:\"%s\"}"
COUNTRY_FORMAT = ":%s {regionName:\"%s\"}"
PLACE_FORMAT = ":Place {cityName:\"%s\", provinceState:\"%s\"}"
SITE_FORMAT = ":Site {Name:\"%s\", Latitude:%f, Longitude:%f, Specificity:\"%s\"}"
ATTACK_FORMAT = ":%s {isSuccessful:\"%s\", involvesSuicide:\"%s\", weaponType:\"%s\", weaponSubType:\"%s\", weaponDetail:\"%s\"}"
FORMAT_TARGET = ":%s {targetSubType:\"%s\", specificVictim:\"%s\", corporateEntity:\"%s\", nationality:\"%s\"}"
FORMAT_PERP = ":Perpetrator {groupName:\"%s\", groupSubName:\"%s\", groupCertainty:\"%s\", numberOfPerpetrators:%d, numberCaptured:%d, claimMode:\"%s\"}"
FORMAT_VICTIMS = ":Victims {Numkilled:%d, USFatalities:%d, terroristsFatalities:%d, NumWounded:%d, USWounded:%d, terroristsWounded:%d}"
FORMAT_PROPERTYDAMAGE = ":PropertyDamage {damageExtent:\"%s\", value:%d, description:\"%s\"}"
FORMAT_HOSTAGESKIDNAPS = ":HostagesKidnaps {Number:%d, USHostageKidnaps:%d, hours:%d, days:%d, ransomAmount:%d, USRansomAmount:%d, ransomAmountPaid:%d, USRansomAmountPaid:%d, numberRealeased:%d, Outcome:\"%s\"}"

FORMAT_CREATE_NODE = "CREATE (n%s);"
FORMAT_CREATE_REL = "MATCH (n%s), (m%s) CREATE (n)-[:%s]->(m);"

def calOffset(s):
  ret = 0
  for c in s:
    ret *= 26
    ret += ord(c) - ord('A') + 1
  return ret-1

########################################################
#helpler function
def getEventStr(row):
  cells = list(row)
  return EVENT_FORMAT % ( \
    int(getCellInt(cells[calOffset("A")])), \
    int(getCellInt(cells[calOffset("B")])), \
    int(getCellInt(cells[calOffset("C")])), \
    int(getCellInt(cells[calOffset("D")])), \
    str(getCellStr(cells[calOffset("E")])), \
    int(getCellInt(cells[calOffset("F")])), \
    str(getCellDate(cells[calOffset("G")])), \
    str(getCellStr(cells[calOffset("S")])), \
    str(getCellStr(cells[calOffset("BM")])), \
    int(getCellInt(cells[calOffset("W")])), \
    str(getCellStr(cells[calOffset("Y")])), \
    int(getCellInt(cells[calOffset("Z")])), \
    str(getCellStr(cells[calOffset("DU")])) \
    )

def getCountryStr(row):
  return COUNTRY_FORMAT % ( \
    str(formatStrForLabel(getCellStr(row[calOffset("I")]))), \
    str(getCellStr(row[calOffset("K")])) \
    )

def getDiversionCountryStr(row):
  countryStr = getCellStr(row[calOffset("DJ")])
  if countryStr == "":
    return ""
  else:   
    return ":%s" % (str(formatStrForLabel(countryStr)))

def getResolutionCountryStr(row):
  countryStr = getCellStr(row[calOffset("DK")])
  if countryStr == "":
    return ""
  else:   
    return ":%s" % (str(formatStrForLabel(countryStr)))

def getPlaceStr(row):
  return PLACE_FORMAT % ( \
    str(getCellStr(row[calOffset("M")])), \
    str(getCellStr(row[calOffset("L")])) \
    )

def getSiteStr(row):    
  specifyStr = "";
  specifyRange = ["", "exact location", "local centroid", "subnational centroid", "administrative centroid", "not really known"]
  specifyInt = getCellInt(row[calOffset("P")])
  if specifyInt != -9999:
    specifyStr = specifyRange[specifyInt]

  return SITE_FORMAT % ( \
    str(getCellStr(row[calOffset("R")])), \
    float(getCellFloat(row[calOffset("N")])), \
    float(getCellFloat(row[calOffset("O")])), \
    str(specifyStr) \
    )   

# function to gen attack fraction, based on attack type and weapon type
def getAttackStr(row, attackStr, weaponStr, weaponSubStr):    
  succStr = "true"  #isSuccessful
  if row[calOffset("AA")] is 0:
    succStr = "false"
  suicStr = "true"   #involvesSuicide
  if row[calOffset("AB")] is 0:
    suicStr = "false"
  return ATTACK_FORMAT % ( \
    str(attackStr), \
    str(succStr), \
    str(suicStr), \
    str(weaponStr), \
    str(weaponSubStr), \
    str(getCellStr(row[calOffset("CS")])) \
    )

def getTargetStr(targetStr, targetSubStr, victimStr, corpStr, natltyStr):
  return FORMAT_TARGET % (targetStr, targetSubStr, victimStr, corpStr, natltyStr)

def getPerpetratorStr(row, gnameStr, gnameSubStr, gcertCellStr):
  gcertStr = "confirmed"
  if row[calOffset(gcertCellStr)] is 0:
    gcertStr = "unconfirmed"
  claimModeStr = getCellStr(row[calOffset("BU")])
  if claimModeStr is "":
    claimModeStr = "not claimed"
  return FORMAT_PERP % ( \
    gnameStr, \
    gnameSubStr, \
    gcertStr, \
    int(getCellInt(row[calOffset("BQ")])), \
    int(getCellInt(row[calOffset("BR")])), \
    claimModeStr \
    )

def getVictimsStr(row):
  return FORMAT_VICTIMS % ( \
    int(getCellInt(row[calOffset("CT")])), \
    int(getCellInt(row[calOffset("CU")])), \
    int(getCellInt(row[calOffset("CV")])), \
    int(getCellInt(row[calOffset("CW")])), \
    int(getCellInt(row[calOffset("CX")])), \
    int(getCellInt(row[calOffset("CY")])) \
    )
    
def getPropertyDamageStr(row):
  return FORMAT_PROPERTYDAMAGE % ( \
    str(getCellStr(row[calOffset("DB")])), \
    int(getCellInt(row[calOffset("DC")])), \
    str(getCellStr(row[calOffset("DD")])) \
    )

def getHostagesKidnapsStr(row):
  return FORMAT_HOSTAGESKIDNAPS % (\
    getCellInt(row[calOffset("DF")]), \
    getCellInt(row[calOffset("DG")]), \
    getCellInt(row[calOffset("DH")]), \
    getCellInt(row[calOffset("DI")]), \
    getCellInt(row[calOffset("DM")]), \
    getCellInt(row[calOffset("DN")]), \
    getCellInt(row[calOffset("DO")]), \
    getCellInt(row[calOffset("DP")]), \
    getCellInt(row[calOffset("DT")]), \
    getCellStr(row[calOffset("DS")]) \
  )
